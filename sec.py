import math


def isPrime(n):
    if n <= 1:
        return False
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True


# 用辗转相除求最大公因子
def gcd(a, b):
    r = a % b
    while (r != 0):
        a = b
        b = r
        r = a % b
    return b


# 欧拉函数-暴力循环版
def euler(a):
    count = 0
    for i in range(1, a):
        if gcd(a, i) == 1:
            count += 1
    return count


def order(a, n, b):
    #   输出b在mod(a)中的阶
    #   n是mod(a)群的阶
    p = 1
    while (p <= n and (b**p % a != 1)):
        p += 1
    if p <= n:
        return p
    else:
        return -1


def ind(g, a, m):
    #输出指标ind g(a) mod(m)
    for i in range(1, m):
        if (g**i) % m == a:
            return i


# 简化剩余系
def simple(a):
    b = []
    for i in range(1, a):
        if gcd(i, a) == 1:
            b.append(i)
    return b


# 求任意数原根
def root(a):
    n = euler(a)
    prim = []
    for b in range(2, a):
        if order(a, n, b) == n:
            prim.append(b)
    print(prim)


#扩展欧几里得算法,返回值列表中,x是a的逆元(mod b),q是gcd(a,b),若x是0,则表示没有逆元
#y是计算过程中的迭代的参数,可以不用管
#此算法实质上是广义欧几里得除法的逆运算,用递归可以体现出这个逆运算的过程
def exgcd(a, b):
    if 0 == b:
        x = 1
        y = 0
        q = a
        return x, y, q
    xyq = exgcd(b, a % b)
    x = xyq[0]
    y = xyq[1]
    q = xyq[2]
    temp = x
    x = y
    y = temp - a // b * y
    return x, y, q


#获取a的逆元(mod b)的函数，目的是为了封装获取逆元的功能
def Get_Inverse(a, b):
    return exgcd(a, b)[0]


#判断所有的mi是否两两互质
def Is_Coprime(m_list):
    for i in range(len(m_list)):
        for j in range(i + 1, len(m_list)):
            if 1 != gcd(m_list[i], m_list[j]):
                return 0  #返回0表示不是两两互质的
    return 1  #返回1表示是两两互质的


#获取所有的Mi
def Get_Mi(m_list, M):
    Mi_list = []
    for mi in m_list:
        Mi_list.append(M // mi)
    return Mi_list


#获取所有的Mi的逆元
def Get_Mi_inverse(Mi_list, m_list):
    Mi_inverse = []
    for i in range(len(Mi_list)):
        Mi_inverse.append(Get_Inverse(Mi_list[i], m_list[i]))
    return Mi_inverse


#中国剩余定理,返回值为最终的x
def crt():
    while True:
        #两个空列表，分别用来保存mi和bi
        m_list = []
        b_list = []

        while True:  #输入mi
            m_i = input("mi:")
            if m_i == '.':
                break
            elif False == m_i.isnumeric():
                print("error:")
                m_i = input()
                continue
            else:
                m_list.append(int(m_i))

        while True:  #输入bi
            b_i = input("bi:")
            if b_i == '.':
                break
            elif False == b_i.isnumeric():
                print("error！\n")
                b_i = input()
                continue
            else:
                b_list.append(int(b_i))

        if len(m_list) != len(b_list):
            print("mi != bi\n")
        elif 0 == Is_Coprime(m_list):
            print("(mi,mi) != 1\n")
        else:
            break

    M = 1  #M是所有mi的乘积
    for mi in m_list:
        M *= mi

    Mi_list = Get_Mi(m_list, M)
    Mi_inverse = Get_Mi_inverse(Mi_list, m_list)
    x = 0
    for i in range(len(b_list)):  #开始计算x
        x += Mi_list[i] * Mi_inverse[i] * b_list[i]
        x %= M
    return x


if __name__ == '__main__':
    print("result=%d" % crt())
